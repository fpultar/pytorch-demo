#include <torch/csrc/autograd/function.h>
#include <torch/csrc/autograd/variable.h>
#include <torch/script.h> // One-stop header.

#include <cassert>
#include <cinttypes>
#include <iostream>
#include <memory>
#include <tuple>
#include <vector>

int main(int argc, const char *argv[]) {
  if (argc != 2) {
    std::cerr << "usage: example-app <path-to-exported-script-module>\n";
    return -1;
  }

  // read in model
  torch::jit::script::Module module;
  try {
    // Deserialize the ScriptModule from a file using torch::jit::load().
    module = torch::jit::load(argv[1]);
  } catch (const c10::Error &e) {
    std::cerr << "error loading the model\n";
    return -1;
  }

  // torch::Tensor from C array
  auto tensor_float_no_grad =
      torch::TensorOptions() // specify data type, where the tensor
                             // will live, gradients, ...
          .dtype(torch::kFloat32)
          .device(torch::kCPU)
          .layout(torch::kStrided);

  auto tensor_float_grad =
      torch::TensorOptions() // specify data type, where the tensor
                             // will live, gradients, ...
          .dtype(torch::kFloat32)
          .device(torch::kCPU)
          .layout(torch::kStrided)
          .requires_grad(true);

  auto tensor_int = torch::TensorOptions() // specify data type, where the
                                           // tensor will live, gradients, ...
                                               .dtype(torch::kInt32)
                                               .device(torch::kCPU)
                                               .layout(torch::kStrided);

  // convert to torch::Tensor from std::vector - seems like better approach

  int const natoms = 31;
  int const ncharges = 32;
  int const dims = 3;
  int const batch_size = 1;

  std::vector<u_int32_t> qm_atomic_numbers = {1, 6, 1, 1, 6, 1, 6, 1, 1, 1, 6,
                                              1, 6, 1, 1, 6, 1, 1, 6, 1, 6, 1,
                                              1, 1, 6, 1, 1, 6, 1, 8, 1};

  std::vector<float> qm_positions = {
      -0.442947496, 11.001947210, 23.53843018,  -2.236268461, 11.818985980,
      22.93889444,  -1.841717705, 13.792091510, 22.49830981,  -2.896722482,
      10.768954200, 21.29444454,  -4.133665133, 11.695821690, 25.11713090,
      -4.513283393, 9.713842463,  25.52960654,  -6.643232696, 12.675979330,
      24.06753249,  -6.497152995, 14.672778630, 23.58352390,  -8.148942688,
      12.705927690, 25.47277389,  -7.353363183, 11.592564650, 22.46591770,
      -3.249632431, 12.876986410, 27.66935330,  -4.846956553, 12.734322190,
      28.99201199,  -2.467110848, 15.660343880, 27.33890614,  -4.009123687,
      16.787220770, 26.56744596,  -0.818133300, 15.913359350, 26.13075930,
      -1.719833938, 16.708905520, 29.92782456,  -1.175383185, 18.674510890,
      29.63963875,  -3.270175176, 16.843998070, 31.30603239,  0.393734165,
      15.240980550, 31.24575662,  2.098110773,  15.276181590, 30.05626854,
      1.103721260,  16.300985820, 33.84032285,  -0.510552700, 16.227748650,
      35.11752635,  1.786206150,  18.232492740, 33.62584759,  2.541275947,
      15.177545230, 34.79620160,  -0.431661103, 12.490133300, 31.57724434,
      1.201728308,  11.377478740, 32.22095010,  -1.982252711, 12.263731930,
      32.94292201,  -1.094745099, 11.448025710, 28.96323675,  0.563579412,
      11.458509150, 27.70991388,  -1.947354387, 8.933606299,  29.46637609,
      -0.489290309, 7.918137207,  29.92393411};

  std::vector<u_int32_t> mm_atomic_numbers = {7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7,
                                              7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7,
                                              7, 7, 7, 7, 7, 7, 7, 7, 7, 7};

  std::vector<float> mm_charges = {
      0.431000,  -0.664000, 0.173000, 0.020000, 0.020000,  0.020000,  0.431000,
      -0.664000, 0.173000,  0.020000, 0.020000, 0.020000,  0.431000,  -0.664000,
      0.173000,  0.020000,  0.020000, 0.020000, 0.431000,  -0.664000, 0.173000,
      0.020000,  0.020000,  0.020000, 0.431000, -0.664000, 0.173000,  0.020000,
      0.020000,  0.020000,  0.431000, -0.664000};

  std::vector<float> mm_positions = {
      -1.696669514,  28.11745897,  55.50995136,   -0.967547429, 28.88443423,
      54.00850230,   -0.950868672, 31.58534217,   53.92332839,  0.439341438,
      32.17158366,   52.52085582,  -0.346867500,  32.42710162,  55.70375073,
      -2.886638164,  32.14280874,  53.49308978,   26.383000520, 21.74765157,
      24.17099786,   27.036716710, 22.30632379,   22.54790876,  26.261114830,
      20.54528062,   20.65041197,  27.011658890,  18.66770536,  21.04304684,
      24.203790340,  20.57032899,  20.55244459,   26.920273440, 21.10742805,
      18.78164663,   25.713072340, 18.66022959,   28.70604561,  26.111998120,
      18.26958272,   26.95615185,  27.664033370,  16.09612865,  26.54280365,
      29.614523860,  16.44665278,  27.10468085,   26.659753370, 14.55538454,
      27.47032282,   27.860595530, 15.78003352,   24.51695451,  12.692343200,
      -11.99272547,  35.30000333,  11.005978380,  -11.84086488, 36.01217832,
      9.538990432,   -10.94052003, 33.92895508,   10.179104340, -11.53047861,
      32.06199633,   9.248569167,  -8.901943552,  33.87774529,  7.688508235,
      -11.83241211,  34.08195518,  -11.936904730, 22.39399025,  52.97048306,
      -10.467911780, 21.31627534,  53.20385904,   -8.951231490, 21.02794412,
      50.98626089,   -8.320084347, 22.92510643,   50.49100234,  -10.224293460,
      20.31250935,   49.53331363,  -7.443136243,  19.66216255,  51.30727310,
      15.373924870,  30.20492464,  53.87317117,   15.097532960, 31.53390939,
      52.63558511};

  torch::Tensor qm_atomic_numbers_tensor = torch::from_blob(
      qm_atomic_numbers.data(), {batch_size, natoms}, tensor_int);
  torch::Tensor qm_positions_tensor = torch::from_blob(
      qm_positions.data(), {batch_size, natoms, dims}, tensor_float_grad);
  torch::Tensor mm_atomic_numbers_tensor = torch::from_blob(
      mm_atomic_numbers.data(), {batch_size, ncharges}, tensor_int);
  torch::Tensor mm_charges_tensor = torch::from_blob(
      mm_charges.data(), {batch_size, ncharges}, tensor_float_grad);
  torch::Tensor mm_positions_tensor = torch::from_blob(
      mm_positions.data(), {batch_size, ncharges, dims}, tensor_float_grad);

  std::cout << "-----------------------------------------" << std::endl;
  std::cout << "QM Atomic Numbers Tensor:" << std::endl;
  std::cout << "-----------------------------------------" << std::endl;
  std::cout << qm_atomic_numbers_tensor << std::endl;

  std::cout << "-----------------------------------------" << std::endl;
  std::cout << "QM Positions Tensor:" << std::endl;
  std::cout << "-----------------------------------------" << std::endl;
  std::cout << qm_positions_tensor << std::endl;

  std::cout << "-----------------------------------------" << std::endl;
  std::cout << "MM Atomic Numbers Tensor:" << std::endl;
  std::cout << "-----------------------------------------" << std::endl;
  std::cout << mm_atomic_numbers_tensor << std::endl;

  std::cout << "-----------------------------------------" << std::endl;
  std::cout << "MM Charges Tensor:" << std::endl;
  std::cout << "-----------------------------------------" << std::endl;
  std::cout << mm_charges_tensor << std::endl;

  std::cout << "-----------------------------------------" << std::endl;
  std::cout << "MM Positions Tensor:" << std::endl;
  std::cout << "-----------------------------------------" << std::endl;
  std::cout << mm_positions_tensor << std::endl;

  // create tupple
  std::tuple<torch::Tensor, torch::Tensor, torch::Tensor, torch::Tensor,
             torch::Tensor>
      input_model_vector(qm_atomic_numbers_tensor, qm_positions_tensor,
                         mm_atomic_numbers_tensor, mm_charges_tensor,
                         mm_positions_tensor);

  auto energy_tensor = module.forward({input_model_vector}).toTensor();
  energy_tensor.backward();

  auto qm_forces_tensor = qm_positions_tensor.grad();
  auto mm_forces_tensor = mm_positions_tensor.grad();

  std::cout << "-----------------------------------------" << std::endl;
  std::cout << "Energy Tensor: " << std::endl;
  std::cout << "-----------------------------------------" << std::endl;
  std::cout << energy_tensor << std::endl;

  std::cout << "-----------------------------------------" << std::endl;
  std::cout << "Forces on QM Tensor: " << std::endl;
  std::cout << "-----------------------------------------" << std::endl;
  std::cout << qm_forces_tensor << std::endl;

  std::cout << "-----------------------------------------" << std::endl;
  std::cout << "Forces on MM Tensor: " << std::endl;
  std::cout << "-----------------------------------------" << std::endl;
  std::cout << mm_forces_tensor << std::endl;
}
